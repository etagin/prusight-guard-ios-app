import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import AuthNavigation from "./src/navigations/AuthNavigation";
import AppNavigation  from "./src/navigations/AppNavigation";
import VisitorDataScreen from "./src/screens/HomeScreen";

export default createAppContainer(


  createSwitchNavigator(
    {   
    
      Auth: AuthNavigation,
      App: AppNavigation,
      visitorData : VisitorDataScreen,
    },
    {     
      initialRouteName: 'Auth',
    }
  )
);

// export default createAppContainer(navigator);



