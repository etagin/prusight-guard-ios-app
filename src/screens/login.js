import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Heading} from  '../components/Heading';
import apiConfig from '../api/settings';

const login = ({ navigation }) => {

// state hooks
   
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState(""); 
    const [loading, setLoading] = React.useState(false);
    const [message, setMessage] = useState("");
    const organisationName = navigation.getParam('organization_name');

//login api

  const onUserSignin = async () => {     

    let formdata = new FormData();
    formdata.append('mobile', email);
    formdata.append('pin', password);
    formdata.append('org_id', navigation.getParam('org_id'));

    await apiConfig.post('guard_login', formdata)
    .then((response) => {
        let status = response.data.status;
        let obj = response.data;
        if(status == "1" ){ 
          navigation.navigate('App', obj);         
          // console.log(obj)
          AsyncStorage.setItem('user',JSON.stringify(obj));         
        }
        else if (status == "0") {
          setMessage(obj.result)         
          return;
        }     
    })
    .catch((error) => {
     console.error(error);
  });
}

    return (      
        <View style={styles.container}> 
          <Image style = {styles.BorderClass} source={require('../assests/images/logo-prusight.png')} />
          <View style = {styles.secondheading}>
             <Heading style = {styles.secondheadingtext} >{organisationName}</Heading>
          </View>
          {message ? (<Text style={styles.errorMessage}>{message}</Text>) : null}
         
          <View style={styles.inputView} >
            <TextInput  
              style={styles.inputText}
              placeholder="Enter Your Mobile or Email" 
              placeholderTextColor="#003f5c"
              value={email.toString()}
              onChangeText={(email) => setEmail(email)}            
              />
          </View>
          <View style={styles.inputView} >
            <TextInput  
              secureTextEntry
              style={styles.inputText}
              placeholder="Enter Your Pin" 
              placeholderTextColor="#003f5c"            
              value={password.toString()}
              onChangeText={(password) => setPassword(password)}            
              />
          </View>  
            <TouchableOpacity style={styles.loginBtn}   onPress={() => onUserSignin({ email, password })}>          
              <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
        </View>
    );
}
      //hides the header
      login.navigationOptions = () => {
        return {
          header: () => false,
        };
      };


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#8854d0',
      alignItems: 'center',
      justifyContent: 'center',
    },
    loaderstyle: {
      flex: 1,    
      alignItems: 'center',
      justifyContent: 'center',
    },
    logo:{
      fontWeight:"bold",
      fontSize:50,
      color:"#fb5b5a",
      marginBottom:40
    },
    inputView:{
      width:"80%",
      backgroundColor:"#e8e8e8",
      borderColor: '#F44336',
      borderRadius:25,
      borderWidth: 1,
      height:50,     
      marginVertical: 8,    
      justifyContent:"center",
      padding:20
    },
    inputText:{
      height:50,
      color:"black"
    },
    secondheading:{
      marginTop:20,
      color:"white"
    },
    secondheadingtext:{
      color:"white"
    },
    
    forgot:{
      color:"white",
      fontSize:11
    },
    loginBtn:{
      width:"80%",
      backgroundColor:"#a55eea",      
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:20,
      marginBottom:10
    },
    loginText:{
      color:"white"
    },
    errorMessage: {
      fontSize: 16,
      color: 'red',
      marginLeft: 15,
      marginTop: 15,
    },
    BorderClass:
    {
     resizeMode: 'cover',    
    // Set border color.
    // borderColor: '#F44336',
    }
  });
  
export default login;