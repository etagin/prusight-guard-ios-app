import React, { useState, useEffect} from "react";
import { StyleSheet,View,TouchableOpacity,Image, Text, ScrollView,Modal} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Header, Icon   } from 'react-native-elements';
import TotalVisitorComponent from '../components/visitorCount';
import OtpManualComp from '../components/manualOtp';
import {Button, Snackbar  } from "react-native-paper";

const HomeScreen = ({ navigation }) => {

  // State hooks for setting the navigated data
  const [message, setMessage] = useState("");

  const [visible, setVisible] = React.useState(false);

  const onToggleSnackBar = () => setVisible(!visible);

  const onDismissSnackBar = () => setVisible(false);
  // signout clear
  const signOutAsync = async () => {
    const keys = await AsyncStorage.getAllKeys();
    await AsyncStorage.multiRemove(keys);
    
    // const keys = await AsyncStorage.clear();
    console.log(keys)
    navigation.navigate('Auth');
  };

  const homeMenu = async () => {
    try {      
      navigation.navigate('App');
    }catch (e) {
      alert('Failed to goback In')
    }
  }

  const [modalVisible, setModalVisible] = useState(false);
  const toggleModalVisibility = async () => {    
    setModalVisible(true); 
}; 
const cancel_Btn_handler = async () => {
  try {
    setModalVisible(!modalVisible);
   
  
  }
  catch (e) {
    alert('Failed to fetch data')
  }
}
  return (
    
    <View style={styles.container}> 
    
      {/* header */}
  
        <Header  style={styles.header}   
          // leftComponent={{ icon: 'menu', color: '#fff' , style: {paddingTop: 5, }}}
          leftComponent={{ color: '#fff' }}
          centerComponent={{ text: 'Prusight', style: { color: '#fff', fontSize:20 }, onPress: () => homeMenu() }}
          // rightComponent={{ icon: 'logout',  color: '#fff', onPress: () => signOutAsync() }}
          rightComponent={{ icon: 'logout',  color: '#fff', onPress: () => toggleModalVisibility() }}
          containerStyle={{
            backgroundColor: '#8854d0',    
            // height: 80,
           alignItems: "center",
           paddingTop: 20,
          paddingBottom:20,
          }}
          
        />

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
            {/* <View   style={{              
                width: "100%",
              
              }}> */}
               <View style={styles.modalView}>
               <Text  style={styles.customHeading}>Are you sure you want to logout?</Text>
               <View style={styles.content}>
                  <TouchableOpacity style={styles.customBtn2}   onPress={() => cancel_Btn_handler()}>  
                  <Text style={styles.custom_btn_Text}>CANCEL</Text>
                  </TouchableOpacity>  
                  <TouchableOpacity style={styles.customBtn3}  onPress={() => signOutAsync()} >  
                      <Text style={styles.custom_btn_Text}>LOGOUT</Text>
                  </TouchableOpacity>
               </View>                       
           </View>
        </Modal>
       
        <ScrollView>

        {/* <View>
        {message ? ( 
        <Snackbar
          visible={visible}
          onDismiss={onDismissSnackBar}
          action={{
            label: 'Undo',
            onPress: () => {
              // Do something
            },
          }}>
            
       {message}
        </Snackbar>
        ) : null}
        </View> */}
      
          {message ? (<Text style={styles.errorMessage}>{message}</Text>) : null}
          <View style={styles.section1}>         
            {/* <View  style={styles.scanqrcode} >           */}
              {/* <TouchableOpacity onPress={() => barcodeScan()} > */}
              {/* <TouchableOpacity>
                <Image style={styles.qrcodeimage} source={require('../assests/images/qr-code-scan.png')}  />
              </TouchableOpacity>
              
            </View> */}
                  <OtpManualComp navigation={navigation} />        
          </View>
                  <TotalVisitorComponent navigation={navigation} />    
        </ScrollView>  
    </View>   
  );
};

  //hides the original header
  HomeScreen.navigationOptions = () => {
    return {
      header: () => false,
    };
  };

const styles = StyleSheet.create({
  container: {  
    flex: 1,
        backgroundColor: '#f1f2f6',
        justifyContent: 'flex-start',
        alignItems: 'center',        
    },
  header: {
    // height: 100,
    padding:20,
    // marginTop: StatusBar.currentHeight
  }, 
  errorMessage: {
      fontSize: 25,
      color: '#B53471',
      marginLeft: 15,
      marginTop: 15,
    },
    // otpcss: {     
    //   marginRight: 15,
    //   width:"100%"
    // },
    section1: {      
      flexDirection:"row",
      width: "96%",
      backgroundColor: '#fff',    
      // paddingTop:30,
      paddingBottom:30,
      borderRadius:20,
      borderWidth: 1,
      borderColor: '#eee',
      marginTop:30,
      marginBottom:0,
      alignItems:"center", 
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.3,
      shadowRadius: 1,  
     margin:"2%"

    },
    scanqrcode: {
      paddingTop:40,
      width: "30%",   
      height:150,     
      flex:1,
      alignItems:"center", 
      borderRightColor:"#000",
      borderRightWidth:1
    },
    qrcodeimage: {
      maxWidth:"100%",      
      margin:0,
      alignItems:"center", 
    },
    customBtn2:{
      
      width:"40%",
      backgroundColor:"#27ae60",      
      borderRadius:4,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      margin:0,
      marginRight:20,
      marginTop:0
    },
    customBtn3:{
      
      width:"40%",
      backgroundColor:"#ff4757",      
      borderRadius:4,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      margin:0,
      marginRight:20,
      marginTop:0
    },
    modalView: {
      position:"relative",
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      paddingTop:30 ,
      paddingBottom:30 ,
      paddingLeft:10 ,
      paddingRight:30 ,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    customHeading: {
      marginTop:20,
      fontSize:20,
      textAlign:"center",
      marginBottom:20
    },
    content:{
      paddingLeft:30,
      flex:1,
      marginTop:20,
      marginBottom:20,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center'
  },
  custom_btn_Text:{
    color:"white"
  },
 
});

export default HomeScreen;
