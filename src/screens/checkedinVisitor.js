import React, { useState, useEffect } from "react";
import { StyleSheet,Text,View,ScrollView,TouchableOpacity } from 'react-native';
import { Card, Paragraph, Avatar , Snackbar, Button, ActivityIndicator, Colors } from 'react-native-paper';
import { NavigationActions } from 'react-navigation';
import apiConfig from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
// import LottieView from 'lottie-react-native';
// import { Header, FAB  } from 'react-native-elements';

const checkedinVisitor = ({ navigation }) => {

   // State hooks for setting the navigated data

   const [todaysGaurList, setTodaysGaurList] = useState([]);    
   const [message, setMessage] = useState(""); 
   const [snackvisible, setSnackVisible] = React.useState(false);
   const onDismissSnackBar = () => setSnackVisible(false);
  //  loader
  const [isLoaded, setIsLoaded] = useState(false) ;

  //  useEffect(() => {
  //    checkedInVisitors()
  //  }, []); 

   useEffect(() => {
    
        (async () => {
    
          try {
            const valueString = await AsyncStorage.getItem('user');
            let  valueParsed = JSON.parse(valueString);
            let formdata = new FormData();     
      
            formdata.append('guard_id', valueParsed.guard_id);
            
        
            apiConfig.post('todays_realvisitor', formdata)
              .then((response) => {
            let status = response.data.status;
            let obj = response.data;

            // let  target = new Date("1970-01-01 " + hms);
                if(status == "1" ){
                  
                  setTodaysGaurList(obj.todays_guests);                
                 
                }
                else if (status == "0") {
                  setSnackVisible(!snackvisible)
                  setMessage(obj.message)         
                  return;
                }     
            })
            .catch((error) => {
              setMessage(error)         
            //  console.error(error);
          });
          }
          catch (e) {
            alert('Failed to fetch the data from storage')
          }
       
    
        })();
        // setTodaysGaurList({});
      }, []);
  
    

   const chechkoutindividualvisitor = async (id) => {
   
     try{
      setIsLoaded(true);  
      const valueString = await AsyncStorage.getItem('user');
      let  valueParsed = JSON.parse(valueString);
      let formdata = new FormData(); 
      formdata.append('guard_id', valueParsed.guard_id);
      formdata.append('guest_id', id);

      apiConfig.post('visitors_checkout', formdata)
      .then((response) => {
        // console.log(response.data);
        let status = response.data.status;
        let obj = response.data;
            if(status == "1" ){
              // setIsLoaded(false);
              // checkedInVisitors()
              setSnackVisible(!snackvisible)
              setMessage(obj.visitor_name + " , " + obj.message) 
              alert(obj.visitor_name + " , " + obj.message)
              // console.log("success")
              navigation.navigate(
                'App', 
                {}, 
                NavigationActions.navigate({ 
                    routeName: 'Home' 
                })
            )        
              
            }
              else if (status == "0") {
                // setIsLoaded(false);
                setMessage(obj.message)         
                return;
              }     
            })
        .catch((error) => {
          // setIsLoaded(false);
        console.error(error);
      })
      // .finally(() => setLoading(false));
      
     }
     catch (e) {
      alert('Failed to fetch the data from storage')
    }
  
   }

    

  return (
    
    <View style={styles.container}> 
     {/* {message ? (<Text style={styles.errorMessage}>{message}</Text>) : null} */}     
    
          
    <ScrollView>
    <View >
    {snackvisible ? (    
        <Snackbar
        // style={{position: "absolute", top: 0, right:0, zIndex: 1}}
        top={80}
          // position={top}
          visible={snackvisible}
          onDismiss={onDismissSnackBar}
          duration={5000}          
          >             
         <Text>{message}</Text>
        </Snackbar> ) : null} 
    </View>
    {/* {(() => {
              if(isLoaded){
                return(
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size="large"/>
                     
                    </View>
                );
            }
              
             
            })()} */}
  
    <View style={{ height: "100%", marginTop:0}}>

    {
   
todaysGaurList.map((user, index) => (
    
      <Card key={index} style={styles.row}>
        <Card.Content style={styles.CustomCardDesign}>       
          <View  style={styles.row}>   
          <Avatar.Image style={styles.avtarImageStyle} size={50} source= {{uri:user.image}} />
          </View>
          <View  style={styles.visitTimewrap}>
          <Paragraph style={styles.visitTime}>Visit Time{'\n'}
          {/* {moment.utc(user.visit_time, 'hh:mm A').local().format('hh:mm A')} */}

          {(() => {
              if (user.visit_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.visit_time, 'hh:mm A').local().format('h:mm A');
            })()}

          {/* {user.visit_time}  */}
          </Paragraph>
         
          </View>
        

          <Paragraph><Text style={{ fontWeight:"bold"}}>{user.guest_name} </Text></Paragraph>
          
          <Paragraph  >{user.mobile}</Paragraph>
          <Paragraph  ><Text style={{ color:"#10ac84"}}>Check In :</Text> 
          {/* {moment(user.checkin_time).local().toDate()} */}
          {/* {moment.utc(user.checkin_time, 'hh:mm A').local().format('hh:mm A')} */}

          {(() => {
              if (user.checkin_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkin_time, 'hh:mm A').local().format('h:mm A');
            })()}

         
          </Paragraph>
          <Paragraph ><Text style={{ color:"#ee5253"}}>Check Out :</Text> 
          {/* {user.checkout_time} */}
          {(() => {
              if (user.checkout_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkout_time, 'hh:mm A').local().format('h:mm A');
            })()}
         
          
          </Paragraph>
          <Paragraph ><Text style={{ fontWeight:"bold"}}>Total Guest : </Text>{user.total_guest}</Paragraph>  
          <Button icon="refresh" mode="contained" disabled={isLoaded} icon="refresh" onPress={() => chechkoutindividualvisitor(user.guest_id)} >
          Check Out
          </Button>
            {/* <TouchableOpacity style={styles.loginBtn}  disabled={isLoaded}   onPress={() => chechkoutindividualvisitor(user.guest_id)}>          
              <Text style={styles.loginText}>Check Out</Text>
            </TouchableOpacity>         */}
        </Card.Content>        
      </Card> 
              
    ))} 
    </View> 
     </ScrollView>      
  
    </View> 
  
  );
};
  // hides the header
  // checkedinVisitor.navigationOptions = () => {
  //   return {
  //     header: () => true,
  //   };
  // };

const styles = StyleSheet.create({
  container: {
    position:"relative",
    flex:1,
    fontSize:10,
    backgroundColor: "#353b48",
    textAlign: "center"
  },
  loginBtn:{
    width:"30%",
    backgroundColor:"#353b48",      
    borderRadius:8,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10
  },
  loginText:{
    color:"white"
  },
  text: {
    fontSize: 30
  },
  avtarImageStyle:{
    alignSelf: 'flex-start',
   
  },
  visitTimewrap:{
    
    width:"100%",
    position: 'relative',
    borderBottomColor: 'purple'  
  },
  visitTime:{
    fontSize:12,
    position: 'absolute',
    top: -60,
    right: -8   
  },
  row:{
    
    flexDirection: "row",
    flexWrap: "wrap",
    width:"100%",
    textAlign: 'right',
    borderColor: '#ccc' ,
    backgroundColor: "#f5f6fa" ,
    marginTop:20,
    // marginBottom: 10,
   borderRadius: 10
  },
  logoutBtn:{
    width:"80%",
    backgroundColor:"#a55eea",      
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10
  },
  logoutText:{
    color:"white"
  },
  errorMessage: {
    fontSize: 16,
    color: 'red',
    marginLeft: 15,
    marginTop: 15,
  },
});

export default checkedinVisitor;
