import React, { useState, useEffect } from "react";
import { StyleSheet,Text,View,ScrollView,TouchableOpacity } from 'react-native';
import { Card, Paragraph, Avatar, Snackbar  } from 'react-native-paper';
import apiConfig from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
// import { Header, FAB  } from 'react-native-elements';

const checkedOutVisitor = ({ navigation }) => {

   // State hooks for setting the navigated data

   const [todaysGaurList, setTodaysGaurList] = useState([]);
   const [message, setMessage] = useState("");
   const [snackvisible, setSnackVisible] = React.useState(false);
   const onDismissSnackBar = () => setSnackVisible(false);

  //  useEffect(() => {
  //    checkedOutVisitors()
  //  }, []); 
 
   useEffect(() => {
    
    (async () => { 

      try {
        
        const valueString = await AsyncStorage.getItem('user');
        let valueParsed = JSON.parse(valueString);
        let formdata = new FormData();  
        let stausValue = 4;     
        formdata.append('status ', stausValue);
        formdata.append('org_id', valueParsed.org_id);
        formdata.append('guard_id', valueParsed.org_id);        
        // console.log(valueParsed)
       
        apiConfig.post('todays_gaurd_guest', formdata)
          .then((response) => {       
        let status = response.data.status;
        let obj = response.data;
        let objFilter = response.data.todays_guests;
        // console.log("inside")
       
       
            if(status == "1" ){             
              let filteredArray = objFilter.filter(function(itm){
                return itm.guest_status == "4";                
              });
              // console.log(filteredArray)
              setTodaysGaurList(filteredArray);   
              if(!filteredArray){
                setSnackVisible(!snackvisible)
              setMessage("no data found")  
              }
                     
            }
            else if (status == "0") {
              setSnackVisible(!snackvisible)
              setMessage(obj.message)         
              return;
            }     
        })
        .catch((error) => {
         console.error(error);
      });
      }
      catch (e) {
        alert('Failed to fetch the data from storage')
      }

    })();    
  }, []); 

  return (    
    <View style={styles.container}> 
      <ScrollView>
      <View >
      {/* <Text>{message}</Text> */}
    {snackvisible ? (    
        <Snackbar
        // style={{position: "absolute", top: 0, right:0, zIndex: 1}}
        top={80}
          // position={top}
          visible={snackvisible}
          onDismiss={onDismissSnackBar}
          duration={5000}          
          >             
         <Text>{message}</Text>
        </Snackbar> ) : null} 
    </View>
    <View style={{ height: "100%", marginTop:0}}>
    {todaysGaurList.map((user, index) => (
      <Card key={index} style={styles.row}>
        <Card.Content style={styles.CustomCardDesign}>       
          <View  style={styles.row}>   
          <Avatar.Image style={styles.avtarImageStyle} size={50} source= {{uri:user.image}} />
          </View>
          <View  style={styles.visitTimewrap}>
          <Paragraph style={styles.visitTime}>Visit Time{'\n'}
          {/* {user.visit_time}  */}
          {/* {moment.utc(user.visit_time, 'hh:mm A').local().format('hh:mm A')} */}

          {(() => {
              if (user.visit_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.visit_time, 'hh:mm A').local().format('h:mm A');
            })()}

          </Paragraph>         
          </View>    
          <Paragraph><Text style={{ fontWeight:"bold"}}>{user.guest_name} </Text></Paragraph>
          <Paragraph  >{user.mobile}</Paragraph>
          <Paragraph  ><Text style={{ color:"#10ac84"}}>Check In :</Text> 
          {/* {user.checkin_time} */}
          {/* {moment.utc(user.checkin_time, 'hh:mm A').local().format('hh:mm A')} */}
          {(() => {
              if (user.checkin_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkin_time, 'hh:mm A').local().format('h:mm A');
            })()}
          </Paragraph>
          <Paragraph ><Text style={{ color:"#ee5253"}}>Check Out :</Text> 
          {/* {user.checkout_time} */}
          {/* {moment.utc(user.checkout_time, 'hh:mm A').local().format('hh:mm A')} */}
          {(() => {
              if (user.checkout_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkout_time, 'hh:mm A').local().format('h:mm A');
            })()}
          
          </Paragraph>
          <Paragraph ><Text style={{ fontWeight:"bold"}}>Total Guest : </Text>{user.total_guest}</Paragraph>          
        </Card.Content>        
      </Card>               
    ))} 

    </View>
     
     </ScrollView>      
  
    </View> 
  
  );
};
 
const styles = StyleSheet.create({
  container: {
    position:"relative",
    flex:1,
    fontSize:10,
    backgroundColor: "#353b48",
    textAlign: "center"
  },

  avtarImageStyle:{
    alignSelf: 'flex-start',
   
  },
  visitTimewrap:{
    
    width:"100%",
    position: 'relative',
    borderBottomColor: 'purple'  
  },
  visitTime:{
    fontSize:12,
    position: 'absolute',
    top: -60,
    right: -8   
  },
  row:{    
    flexDirection: "row",
    flexWrap: "wrap",
    width:"100%",
    textAlign: 'right',
    borderColor: '#ccc' ,
    backgroundColor: "#f5f6fa" ,
    marginTop:20,
    // marginBottom: 10,
   borderRadius: 10
  },
});

export default checkedOutVisitor;
