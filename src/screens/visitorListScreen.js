import React, { useState, useEffect } from "react";
import { StyleSheet,Text,View,ScrollView,TouchableOpacity } from 'react-native';
import { Card, Paragraph, Avatar  } from 'react-native-paper';
import apiConfig from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';
// import moment from 'moment';
const moment = require('moment-timezone');
const VisitorListScreen = ({ navigation }) => {

   // State hooks for setting the navigated data

   const [todaysGaurList, setTodaysGaurList] = useState([]);
   const [message, setMessage] = useState("");

  //  useEffect(() => {
  //    todaysGuardList()
  //  }, []); 
  useEffect(() => {
    
    (async () => {

      try {
        const valueString = await AsyncStorage.getItem('user');
        let  valueParsed = JSON.parse(valueString);
        let formdata = new FormData();     
  
        formdata.append('user_id',valueParsed.guard_id);
        formdata.append('org_id', valueParsed.org_id);
      
        apiConfig.post('todays_gaurd_guest', formdata)
          .then((response) => {
        let status = response.data.status;
        let obj = response.data;
            if(status == "1" ){
              setTodaysGaurList(obj.todays_guests);         
              
                  //  console.log(obj)
            }
            else if (status == "0") {
              
              setMessage(obj.message)         
              return;
            }     
        })
        .catch((error) => {
         setMessage(error)    
         // console.error(error);
      });
      }
      catch (e) {
        alert('Failed to fetch the data from storage')
      }


    })();
 
  }, []);
 
   // calling the todays guard list api
//    const todaysGuardList = async () => {
 
//      try {
//        const valueString = await AsyncStorage.getItem('user');
//        let  valueParsed = JSON.parse(valueString);
//        let formdata = new FormData();     
 
//        formdata.append('user_id',valueParsed.guard_id);
//        formdata.append('org_id', valueParsed.org_id);
     
//        apiConfig.post('todays_gaurd_guest', formdata)
//          .then((response) => {
//        let status = response.data.status;
//        let obj = response.data;
//            if(status == "1" ){
//              setTodaysGaurList(obj.todays_guests);
//                   // console.log(obj)
//            }
//            else if (status == "0") {
//              setMessage(obj.message)         
//              return;
//            }     
//        })
//        .catch((error) => {
//         setMessage(error)    
//         // console.error(error);
//      });
//      }
//      catch (e) {
//        alert('Failed to fetch the data from storage')
//      }
  
//  }  


  return (
    
    <View style={styles.container}> 
    {/* header */}
  
   
    <ScrollView>
     {todaysGaurList.map((user, index) => (
      <Card key={index} style={styles.row}>
        <Card.Content style={styles.CustomCardDesign}>       
          <View  style={styles.row}>   
          <Avatar.Image style={styles.avtarImageStyle} size={50} source= {{uri:user.image}} />
          </View>
          <View  style={styles.visitTimewrap}>
          <Paragraph style={styles.visitTime}>Visit Time{'\n'}
          
          {/* {user.visit_time} */}

          {/* {moment.utc(user.visit_time, 'hh:mm A').local().format('h:mm A')} */}

          {(() => {
              if (user.visit_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.visit_time, 'hh:mm A').local().format('h:mm A');
            })()}
          
           </Paragraph>
          {/* <Paragraph style={styles.visitTime}></Paragraph> */}
          </View>
        

          <Paragraph><Text style={{ fontWeight:"bold"}}>{user.guest_name} </Text></Paragraph>
          {/* <Paragraph><Text style={{ fontWeight:"bold"}}>Employee Name:{user.employeename} </Text></Paragraph> */}
          <Paragraph  >{user.mobile}</Paragraph>
          <Paragraph  ><Text style={{ color:"#10ac84"}}>Check In :</Text>
           {/* {user.checkin_time} */}
         
           {/* {moment.utc(user.checkin_time, 'hh:mm A').local().format('hh:mm A')} */}

           {/* {moment.utc(user.visit_date+''+user.checkin_time, 'YYYY-MM-DD hh:mm A').tz('Asia/Kolkata').format('hh:mm A')} */}
           {(() => {
              if (user.checkin_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkin_time, 'hh:mm A').local().format('h:mm A');
            })()}
           
           </Paragraph>
          <Paragraph ><Text style={{ color:"#ee5253"}}>Check Out :</Text>
           {/* {user.checkout_time} */}
           {/* {moment.utc(user.checkout_time, 'hh:mm A').local().format('hh:mm A')} */}
           {(() => {
              if (user.checkout_time == '00:00'){
                  return (
                      <Text>00:00</Text>
                  )
              }
              
              return moment.utc(user.checkout_time, 'hh:mm A').local().format('h:mm A');
            })()}
           {/* {moment.utc(user.checkout_time, 'hh:mm A').local().format('h:mm A')} */}
           {/* {  moment.utc(user.checkout_time, 'hh:mm A').tz('Asia/Kolkata').format('hh:mm A')} */}
           
           </Paragraph>
          <Paragraph ><Text style={{ fontWeight:"bold"}}>Total Guest : </Text>{user.total_guest}</Paragraph>     
          {/* <TouchableOpacity onPress={() => chechkoutindividualVisitor()}>
                <Text  style={styles.visitorBtn}>chechkout Here</Text>
              </TouchableOpacity>         */}
        </Card.Content>        
      </Card> 
              
    ))} 
     </ScrollView>      
  
    </View> 
  
  );
};
  //hides the header
  // VisitorListScreen.navigationOptions = () => {
  //   return {
  //     header: () => false,
  //   };
  // };

const styles = StyleSheet.create({
  container: {
    fontSize:10,
    backgroundColor: "#353b48",
    textAlign: "center"
  },
  loginBtn:{
    width:"20%",
    backgroundColor:"#353b48",      
    borderRadius:8,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10
  },
  loginText:{
    color:"white"
  },
  text: {
    fontSize: 30
  },
  visitorBtn: {
    borderWidth: 1,
    borderColor:"#fff",
    width:"100%",
    padding:10,
    // backgroundColor:"#2ed573",      
    borderRadius:8,
    alignItems:"center",
    justifyContent:"center",
    // marginLeft:20,
    marginTop:10,
    color: '#fff',
  },
  avtarImageStyle:{
    alignSelf: 'flex-start',
   
  },
  visitTimewrap:{
    
    width:"100%",
    position: 'relative',
    borderBottomColor: 'purple'  
  },
  visitTime:{
    fontSize:12,
    position: 'absolute',
    top: -60,
    right: -8   
  },
  row:{
    
    flexDirection: "row",
    flexWrap: "wrap",
    width:"100%",
    textAlign: 'right',
    borderColor: '#ccc' ,
    backgroundColor: "#f5f6fa" ,
    marginTop:20,
    // marginBottom: 10,
   borderRadius: 10
  },
  logoutBtn:{
    width:"80%",
    backgroundColor:"#a55eea",      
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:20,
    marginBottom:10
  },
  logoutText:{
    color:"white"
  },
});

export default VisitorListScreen;
