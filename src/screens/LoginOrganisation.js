import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Button } from 'react-native';
import {Heading} from  '../components/Heading';
import apiConfig from '../api/settings';

const LoginOrganisation = ({ navigation }) => {
 
// state hooks
   
    const [bunit, setBunit] = useState("");
    const [message, setMessage] = useState("");
//Logo api
  // function onUserSignin() {  
  const onUserSubmit = async () => {     

    let formdata = new FormData();
    formdata.append('ref_id', bunit);
   
  await apiConfig.post('logo', formdata)
      .then((response) => {
    let status = response.data.status;
    let obj = response.data;
        if(status == "1" ){      
            // console.log(obj);   
          navigation.navigate('Login', obj);              
        }
        else if (status == "0") {
          setMessage(obj.result)         
          return;
        }     
    })
    .catch((error) => {
     console.error(error);
  });
}

    return (
      
        <View style={styles.container}> 
          <Image style = {styles.BorderClass} source={require('../assests/images/logo-prusight.png')} />
          <View style = {styles.secondheading}>
             <Heading style = {styles.secondheadingtext} >Visitor Management App Guard</Heading>
          </View>
          {message ? (<Text style={styles.errorMessage}>{message}</Text>) : null}
         <View style={styles.inputView} >
            <TextInput  
              style={styles.inputText}
              placeholder="Enter the Organization ID " 
              placeholderTextColor="#003f5c"            
              value={bunit.toString()}
              onChangeText={(bunit) => setBunit(bunit)}
              />
          </View>        
         
            <TouchableOpacity style={styles.loginBtn}   onPress={() => onUserSubmit()}>          
              <Text style={styles.loginText}>SUBMIT</Text>
            </TouchableOpacity>
        </View>
    );
}
      //hides the header
      LoginOrganisation.navigationOptions = () => {
        return {
          header: () => false,
        };
      };


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#8854d0',
      alignItems: 'center',
      justifyContent: 'center',
    },
    loaderstyle: {
      flex: 1,    
      alignItems: 'center',
      justifyContent: 'center',
    },
    logo:{
      fontWeight:"bold",
      fontSize:50,
      color:"#fb5b5a",
      marginBottom:40
    },
    inputView:{
      width:"80%",
      backgroundColor:"#e8e8e8",
      borderColor: '#F44336',
      borderRadius:25,
      borderWidth: 1,
      height:50,     
      marginVertical: 8,    
      justifyContent:"center",
      padding:20
    },
    inputText:{
      height:50,
      color:"black"
    },
    secondheading:{
      marginTop:20,
      color:"white"
    },
    secondheadingtext:{
      color:"white"
    },
    
    forgot:{
      color:"white",
      fontSize:11
    },
    loginBtn:{
      width:"80%",
      backgroundColor:"#a55eea",      
      borderRadius:25,
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:20,
      marginBottom:10
    },
    loginText:{
      color:"white"
    },
    errorMessage: {
      fontSize: 16,
      color: 'red',
      marginLeft: 15,
      marginTop: 15,
    },
    BorderClass:
    {
     resizeMode: 'cover',    
    // Set border color.
    // borderColor: '#F44336',
    }
  });
  
export default LoginOrganisation;