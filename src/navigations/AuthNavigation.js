//AuthNavigation.js
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../screens/login';
import LoginOrganisation from '../screens/LoginOrganisation';

const AuthNavigation = createStackNavigator(
  {
    LoginOrganisation: { screen: LoginOrganisation },
    Login: { screen: Login }
  },
  {
    initialRouteName: 'LoginOrganisation',
    headerMode: 'none'
  }
)

export default AuthNavigation