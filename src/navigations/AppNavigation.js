//AppNavigation.js
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import Home from "../screens/HomeScreen";
import VisitorDataScreen from "../screens/visitorListScreen";
import checkinScreen from "../screens/checkedinVisitor";
import checkoutScreen from "../screens/chechkedOutVisitor";

import AsyncStorage from '@react-native-community/async-storage';
// import Icon from 'react-native-vector-icons'

const AppNavigation = createStackNavigator(
  {
    Home: { screen: Home },
    visitorData : { screen: VisitorDataScreen ,navigationOptions: { title: 'Today Visitors' } },    
    checkedIn : { screen: checkinScreen,  navigationOptions: { title: 'Checked In Visitors' }},    
    checkedOut : { screen: checkoutScreen,  navigationOptions: { title: 'Checked Out Visitors' } },   
  
  },
  {
    // initialRouteName: 'Home'
    defaultNavigationOptions : ({navigation}) => ({
      title : '',
      headerMode: 'none'
    })
  },

)

export default AppNavigation