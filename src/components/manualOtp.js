import React, { useState, useEffect} from "react";
import { StyleSheet,View,TouchableOpacity, Modal,Image, Text, ScrollView,Dimensions,ActivityIndicator} from 'react-native';
import apiConfig from '../api/settings';
import apiConfigdocument from '../api/documentSettings';
import AsyncStorage from '@react-native-community/async-storage';
import { Header, Icon   } from 'react-native-elements';
import CameraModule from '../components/camera';
import ProfileCameraModule from '../components/profilePicCamera';
import { Camera } from "expo-camera";
import { Button, TextInput, Snackbar, Divider } from "react-native-paper";

const manualOtpComponent = ({ navigation }) => {
    const [message, setMessage] = useState("");
    const [responseCode, setResponseCode] = useState("");
    const [responseCodeResult, setResponseCodeResult] = useState("");
    const [CountVisitor, setCount] = useState([]);
    const [value, setValue] = useState({});
    const [image, setImage] = useState(null);
    const [camera, setShowCamera] = useState(false);
    const [hasPermission, setHasPermission] = useState(null);  
    const [isLoaded, setIsLoaded] = useState(false) ;
    const [isDisaprv, setIsDisaprv] = useState(false) ;

    const [profilePiccamera, setProfileShowCamera] = useState(false);
    const [profileImage, setProfielImage] = useState(null);

    // display snackbar

    const [snackvisible, setSnackVisible] = React.useState(false);
    const onDismissSnackBar = () => setSnackVisible(false);

  useEffect(() => {
      (async () => {
        const { status } = await Camera.requestPermissionsAsync();
        setHasPermission(status === "granted");
     
      })();
 
    }, []);

  // if (hasPermission === null) {
  //     return <View />;
  //   }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
      
    }

      // This is to manage Modal State
  const [modalVisible, setModalVisible] = useState(false);
  const toggleModalVisibility = async () => {    
    setModalVisible(true); 
}; 

const cancel_Btn_handler = async () => {
    try {
      setImage("");
      setProfielImage("");
      setModalVisible(!modalVisible);
     
    
    }
    catch (e) {
      alert('Failed to fetch data')
    }
  }

    const chechkCodeScan = async () => {    
      // setImage("");
      // setSnackVisible("")
        // console.log('inside')      
        try {          
          // setIsLoaded(true);
          setIsLoaded(false);  
          setIsDisaprv(false);  
          const valueString = await AsyncStorage.getItem('user');
          let  valueParsed = JSON.parse(valueString);
          let formdata = new FormData();     
    
          formdata.append('guard_id',valueParsed.guard_id);
          formdata.append('response_code', responseCode);          
         
          apiConfig.post('check_code', formdata)
            .then((response) => {   
                        
          let status = response.data.status;
          let obj_response_code = response.data;
          // console.log( response.data)
              if(status == "1" ){ 
                // alert("inside")                           
                setResponseCodeResult(obj_response_code);               
                setCount((parseInt(obj_response_code.total_guest)));               
                toggleModalVisibility();               
              }
              else if (status == "2") {               
                setResponseCode("");
                setSnackVisible(!snackvisible);
                setMessage(obj_response_code.message);  
              
                return;
              } 
              else if (status == "3") {               
                setResponseCode("");
                setSnackVisible(!snackvisible);
                setMessage(obj_response_code.message);  
              
                return;
              } 
              else if (status == "0") {
                setResponseCode("");
                setSnackVisible(!snackvisible)
                setMessage(obj_response_code.message)      
              
                return;
              }     
          })
          .catch((error) => {
           console.error(error);
        });
        }
        catch (e) {
          // setIsLoaded(true);
          alert('Failed to fetch the data from storage')
        }
  }

  const guest_disapprove = async () => {
   
    try {
      setIsDisaprv(true); 
      setIsLoaded(true) ;
    const valueString = await AsyncStorage.getItem('user');
    let  valueParsed = JSON.parse(valueString);
    let formdata = new FormData();
    formdata.append('guest_id',responseCodeResult.guest_id);  
    formdata.append('org_id',valueParsed.org_id);  
 
    apiConfig.post('disapprove', formdata)
    .then((response) => {            
      let status = response.data.status;
      let obj_response_code = response.data;
      // console.log(status)
      if(status == "1" ){
        setIsDisaprv(false);  
        setIsLoaded(false) ;
        setResponseCode("");
        setSnackVisible(!snackvisible)
        setModalVisible(!modalVisible);
        setMessage(obj_response_code.message);
        
        }
      else if (status == "0") {
        setIsDisaprv(false);  
        setIsLoaded(false) ;
        setSnackVisible(!snackvisible)
        setMessage(obj_response_code.message)         
        return;
      }     
  })
  .catch((error) => {
   console.error(error);
});
}
catch (e) {
  setIsDisaprv(false);  
  setIsLoaded(false) ;
  alert('Failed to fetch data')
}
   
  }

  handleInput = (text, index) => { 
    setValue({...value, ["visitor"+index]: text});
  }

 

  const guest_checkin = async () => {
   
    try {
      
     setIsLoaded(true);  
     setIsDisaprv(true); 
     
      const valueString = await AsyncStorage.getItem('user');
      
      let  valueParsed = JSON.parse(valueString);
      let formdata = new FormData();     

      formdata.append('guard_id',valueParsed.guard_id);
      formdata.append('guest_id',responseCodeResult.guest_id);
      formdata.append('checkin_guests', responseCodeResult.total_guest);  
      formdata.append('phone_no', responseCodeResult.guest_mobile);  
      formdata.append('image', profileImage);      
      formdata.append('vaccination_details',JSON.stringify([{"visitor1":"1"}]) );       
     
  
    for (var key in value) {
      
      if (value.hasOwnProperty(key)) {
        if (value[key] > 34 && value[key] < 37.6){
          var temperaturevalues = JSON.stringify(value);  
          // console.log(key + " -> " + value[key]);
         }else{
          var temperaturevalues = 0;  
          setIsLoaded(false);  
          setIsDisaprv(false); 
           alert("Invalid temperature")
         }
          
      }
  }
  if(temperaturevalues==0){
    console.log("invalid temperature")
  }else{
    console.log(temperaturevalues)
    formdata.append('temperature', temperaturevalues);
    apiConfig.post('guest_checkin', formdata)
    .then((response) => {      
      // setIsLoaded(false);      
  let status = response.data.status;
  let obj_response_code = response.data;

      if(status == "1" ){                  
        documentsUpload();  
        setIsLoaded(false);  
        setIsDisaprv(false); 
      setImage("");
      setProfielImage("")
      setResponseCode("");     
      let messageReplace = (obj_response_code.message).replace(", SMS & Email notifications are disabled please contact admin", "");
     
      setMessage(messageReplace);
      console.log(message);
      setSnackVisible(!snackvisible)
      setModalVisible(!modalVisible);

       
      }
      else if (status == "0") {
        setSnackVisible(!snackvisible)
        setMessage(obj_response_code.message)         
        return;
      }    
      
  })
  .catch((error) => {
    setIsLoaded(false);  
    setIsDisaprv(false); 
   console.error(error);
});
    
  }
    
    }
    catch (e) {
     
      alert('Failed to Chechk In')
    }
    
  }
  const documentsUpload = async () => {
   
    try {
      
     
      const valueString = await AsyncStorage.getItem('user');
      let  valueParsed = JSON.parse(valueString);
                
        // console.log(responseCodeResult.guest_id)

        await fetch("http://65.0.95.220/guestpass/Api/documents", {
          method: 'POST',
          headers: {
            
            'Content-Type': 'application/json',
              "apikey": "d29985af97d29a80e40cd81016d939af",
          },         

          body: JSON.stringify({
            "org_id":valueParsed.org_id, 
            "guest_id":responseCodeResult.guest_id,
            "govt_doc":[{"doc_name": "","doc_serial_number": "","doc_image" :image}] 
           })
      })
      
      .then((response) => {
        response.text().then((data) => {
           console.log(data)
        })
    })    
       .catch((error) => {
        console.error(error);
     });

 
    }
    catch (e) {
      alert('Failed to Chechk In')
    }
    
  }

    return (
        <View style={styles.entermanuallycode}>     
      
        {/* {message ? (<Text style={styles.errorMessage}>{message}</Text>) : null}   */}
        <View style={{ height: 30, width:"100%" }}>
        {snackvisible ? (    
        <Snackbar
        // style={{position: "absolute", top: 0, right:0, zIndex: 1}}
        // top={0}
        // position={top}
          visible={snackvisible}
          onDismiss={onDismissSnackBar}
          duration={5000}          
          >             
         <Text>{message}</Text>
         {/* <Text>Visitor Checkedin successfully'.SMS & Email notifications are disabled please contact admin.</Text> */}
        </Snackbar> ) : null} 

        </View>
        
       
               
        <View  style={styles.inputView} >
          <TextInput  
            style={styles.inputText}
            // keyboardType={'numeric'}
            style={styles.inputText}
            placeholder="Enter OTP Manually" 
            placeholderTextColor="#003f5c"
            value={responseCode.toString()}
            onChangeText={(responseCode) => setResponseCode(responseCode)}            
          />          
         </View>     
        <TouchableOpacity style={styles.customBtn}   onPress={() => chechkCodeScan()}>          
      
                <Text style={styles.custom_btn_Text}>NEXT</Text>
        </TouchableOpacity> 
        <Modal
       animationType="slide"
       transparent={true}
       visible={modalVisible}
       onRequestClose={() => {
         Alert.alert("Modal has been closed.");
         setModalVisible(!modalVisible);
       }}
     >
        {/* {(() => {
              if(isLoaded){
                return(
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size="large"/>
                     
                    </View>
                );
            }
              
             
            })()} */}
       <View style={styles.centeredView}>
         <ScrollView>
         <View style={styles.modalView}>
               <TouchableOpacity style={styles.cancelBtn}  onPress={() => cancel_Btn_handler()} >  
                   <Text style={styles.custom_btn_Text}><Icon  name='close' color='#fff'/></Text>
               </TouchableOpacity>   
           <View>
           <Text  style={styles.customHeading}>Please Enter Visitor Details</Text>
           <Text  style={styles.customNote}>Note: Please check valid identity proof of Visitor Before Check in</Text>
         
          <View style={{  justifyContent: "center", alignItems: "center"}}>
          <Text  style={styles.customHeading}>Document Capture</Text>
            <View
              style={{
                backgroundColor: "#eeee",
                width: 70,
                height: 70,
                borderRadius: 100,
                marginBottom: 8,
              }}
            >         
           {/* <Text>{image}</Text>  */}
         
            <Image source={{uri: `data:image/jpeg;base64,${image}`}}  style={{ width: 70, height: 70, borderRadius: 100 }}/>
       
        </View>
        <Button style={styles.cameraBtn} icon="camera" mode="contained"  onPress={() => {
              setShowCamera(true);
            }}>
         Take Document Picture
        </Button>
        

      {camera && (
          <CameraModule
            showModal={camera}
            setModalVisible={() => setShowCamera(false)}
            setImage={(result) => setImage(result.base64)}
            // setImageBase64={(result2) => setImageBase64(result2.base64)}
          />
        )}
      </View>

          {/*start here  */}
          <View style={{  justifyContent: "center", alignItems: "center"}}>
          <Text  style={styles.customHeading}>Add Profile Picture</Text>
            <View
              style={{
                backgroundColor: "#eeee",
                width: 70,
                height: 70,
                borderRadius: 100,
                marginBottom: 8,
              }}
            >         
           {/* <Text>{image}</Text>  */}
         
            <Image source={{uri: `data:image/jpeg;base64,${profileImage}`}}  style={{ width: 70, height: 70, borderRadius: 100 }}/>
       
        </View>
        <Button style={styles.cameraBtn} icon="camera" mode="contained"  onPress={() => {
              setProfileShowCamera(true);
            }}>
         Take Profile Picture
        </Button>
        

      {profilePiccamera && (
          <ProfileCameraModule
            showModal={camera}
            setModalVisible={() => setProfileShowCamera(false)}
            setProfielImage={(resultProfile) => setProfielImage(resultProfile.base64)}
            // setImageBase64={(result2) => setImageBase64(result2.base64)}
          />
        )}
      </View>
         
           </View>
          
               {

                [...Array(CountVisitor)].map((val, index) => (                   
                 <View  style={styles.inputView}  key={index}  >    
                 <TextInput                   
                 style={styles.inputText}
                 placeholder="Visitor Temperature" 
                 placeholderTextColor="#003f5c"                  
                
                 onChangeText={text => handleInput(text, index+1)}                
               /> 
                
                </View> 
               
                 ) 
                )           
              
               }
             
             
           <View style={styles.content}>
           <Button mode="contained" style={styles.customBtn2}  disabled={isLoaded} onPress={() => guest_checkin()} >
           CHECK IN
          </Button>
              
          <Button mode="contained" style={styles.customBtn3}  disabled={isDisaprv}  onPress={() => guest_disapprove()} >
               DISAPPROVE
          </Button>
               {/* <TouchableOpacity style={styles.customBtn3}  onPress={() => guest_disapprove()} >  
                   <Text style={styles.custom_btn_Text}>DISAPPROVE</Text>
               </TouchableOpacity> */}
                       
           </View>
         
         </View>
         </ScrollView>  
       </View>
       
     </Modal>
        
      </View>
  
     
      
        );
}
const styles = StyleSheet.create({
    container: {  
      flex: 1,
          backgroundColor: '#f1f2f6',
          justifyContent: 'flex-start',
          alignItems: 'center',
          
    },
    header: {
      // height: 100,
      padding:20,
    }, 
    text: {
      fontSize: 30
    }, 
    content:{
      paddingLeft:30,
      flex:1,
      marginTop:20,
      marginBottom:20,
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center'
  },
  cameraBtn:{
    marginBottom:20,
  },
  customBtn2:{
      color:"white",
    width:"48%",
    backgroundColor:"#27ae60",      
    borderRadius:4,
    // height:50,
    alignItems:"center",
    justifyContent:"center",
    margin:0,
    marginRight:"1%",
    marginTop:0
  },
  wrapperCustom:{     
    width:"100%",
    alignItems:"center",
    // justifyContent:"center",
    margin:0,    
    marginTop:0
  },
  customBtn3:{
      
    width:"58%",
    backgroundColor:"#ff4757",      
    borderRadius:4,
    // height:50,
    alignItems:"center",
    justifyContent:"center",
    margin:0,
    marginLeft:"1%",
    marginTop:0
  },
    cancelBtn:{
   position:"absolute",
   top:14,   
   right:0,
    width:"10%",
    backgroundColor:"#ff4757",      
    borderRadius:4,
    height:30,
    alignItems:"center",
    justifyContent:"center",
    margin:0,
    marginRight:20,
    marginTop:0,
    paddingTop:5
  },
    customBtn:{    
      width:"100%",
      backgroundColor:"#130f40",      
      borderRadius:4,
      height:50,   
      alignItems:"center",
      justifyContent:"center",
      marginLeft:20
    },
    custom_btn_Text:{
      color:"white"
    },
    content2:{
      // paddingLeft:30,
      // flex:1,
      marginTop:20,
      marginBottom:20,
    
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
     
  },
    inputView2:{
      width:"48%",
      // backgroundColor:"#e8e8e8",
      // borderColor: '#000',
      // borderRadius:4,
      // borderWidth: 1,
      marginLeft:"1%",
      marginRight:"1%",
      // marginBottom:20,
      // height:50,     
      // // marginVertical: 8,    
      // justifyContent:"center",
      // padding:20
    },
    inputView:{
      width:"100%",
      // backgroundColor:"#e8e8e8",
      // borderColor: '#000',
      // borderRadius:4,
      // borderWidth: 1,
      marginLeft:20,
      marginBottom:20,
      height:50,     
      // marginVertical: 8,    
      justifyContent:"center",
      // padding:20
    },
    inputText:{
      height:50,
      color:"black"
    },
    modalView: {
      position:"relative",
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      paddingTop:30 ,
      paddingBottom:30 ,
      paddingLeft:10 ,
      paddingRight:30 ,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    customHeading: {
      marginTop:20,
      fontSize:20,
      textAlign:"center",
      marginBottom:20
    },
    customNote: {
      fontSize:16,
      color:"#747d8c",
      marginBottom:30,
      textAlign:"center",
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#c0392b",
      width:"40%",
      height:50,
      alignItems:"center",
      justifyContent:"center",
      marginTop:20
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    errorMessage: {
        fontSize: 14,
        color: '#B53471',
        marginLeft: 15,
        marginTop: 15,
        marginBottom:20
      },
      section1: {
        // flex:1,
        flexDirection:"row",
        width: "96%",
        backgroundColor: '#fff',
        // paddingLeft:15,
        // paddingRight:15,
        paddingTop:30,
        paddingBottom:30,
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#eee',
        marginTop:30,
        marginBottom:0,
        alignItems:"center", 
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 1,  
  
      },
      scanqrcode: {
        paddingTop:40,
        width: "30%",   
        height:150,
        // paddingLeft:10,
        flex:1,
        alignItems:"center", 
        borderRightColor:"#000",
        borderRightWidth:1
         
        // borderColor: '#eee',
  
      },
      qrcodeimage: {
        maxWidth:"100%",      
        margin:0,
        alignItems:"center", 
      },
      entermanuallycode: {
        flex:1,     
        // position:"relative",
        // zIndex: 0,
        paddingLeft:10, 
        paddingRight:30,
        alignItems:"center",       
  
      },
      section2: {
       
        flexDirection:"row",
        width: "96%",
        // backgroundColor: '#fff',     
        paddingTop:0,
        paddingBottom:0,
        borderRadius:20,
        // borderWidth: 1,
        // borderColor: '#95a5a6',
        marginTop:30,
        marginBottom:0,
        alignItems:"center", 
        
  
      },
      customCol: {
        borderRadius:8,
        elevation: 5,
        backgroundColor: '#130f40',
        width: "31%",
        paddingLeft:"2%",
        paddingRight:"2%",
        marginTop:0,
        marginRight:"1%",
        marginLeft:"1%",
        padding:20,
        marginBottom:0,
        alignItems:'center',
      justifyContent:'center',
      shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 1,  
      },
   
  
  
     
  });
export default manualOtpComponent;

