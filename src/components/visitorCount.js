import React, { useState, useEffect } from "react";
import { StyleSheet,View,TouchableOpacity,Text } from 'react-native';
import apiConfig from '../api/settings';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions } from 'react-navigation';

const VisitorCount = ({ navigation }) => {
    const [TodaysVisitorCount, setTodaysVisitorCount] = useState("");
    const [chechkedInCount, setChechkedInCount] = useState(""); 
    const [chechkedOutCount, setChechkedOutCount] = useState("");
    const [data, setData] = useState([]);
    // const [loading, setLoading] = useState(false);
   
    useEffect(() => {   
      // setLoading(true);
      checkedInVisitors();  
      // if (componentMounted){         
      //   setLoading(false); 
      // }
        // console.log(data)
      }, [data]);      
    
      const checkedInVisitors = async () => {    
      
          try {
            
            const valueString = await AsyncStorage.getItem('user');
            let  valueParsed = JSON.parse(valueString);
            let formdata = new FormData();    
      
            formdata.append('guard_id', valueParsed.guard_id);
            formdata.append('org_id', valueParsed.org_id);
            
            apiConfig.post('total_visitors', formdata)
              .then((response) => {
            let status = response.data.status;
            let obj = response.data;      
            // console.log(obj);
             if(status == "1" ){            
                 
              setData(obj);
                  setTodaysVisitorCount(obj.total_visitors);                   
                  setChechkedInCount(obj.realtime_visiors_count);                   
                  setChechkedOutCount(obj.checkout_count);                   
                }
                else if (status == "0") {
                  setMessage(obj.result) ;                           
                  return;
                }     
            })
            .catch((error) => {
             console.error(error);
          });
          }
          catch (e) {
            alert('Failed to fetch the checkedInVisitors count data from storage')
          }
       
      }

      
      const visitorToday = () => {
        try{
              navigation.navigate(
              'App', 
              {}, 
              NavigationActions.navigate({ 
                  routeName: 'visitorData' 
              })
          )
        }
        catch (e) {
      alert('Failed to fetch data')
    }
      }
      const chechkInVisitor =  () => {
        try{
              navigation.navigate(
              'App', 
              {}, 
              NavigationActions.navigate({ 
                  routeName: 'checkedIn' 
              })
          )
        }
        catch (e) {
      alert('Failed to fetch data')
    }
      }
      const chechkoutVisitor =  () => {
        
        try{
          // console.log("inside2");
              navigation.navigate(
              'App', 
              {}, 
              NavigationActions.navigate({ 
                  routeName: 'checkedOut' 
              })
          )
        }
        catch (e) {
      alert('Failed to fetch data')
    }
      }


  return (
    <View style={styles.section2}>   
       
    <View  style={styles.customCol} >
   
      <Text  style={styles.VisitorscustomHeading}>Today Visitors</Text>
      <Text style={styles.VisitorscustomCount}>{TodaysVisitorCount}</Text>
      <TouchableOpacity onPress={() => visitorToday()}>
        <Text  style={styles.visitorBtn}>VIEW</Text>
      </TouchableOpacity>            
    </View>
    <View  style={styles.customCol} >
      <Text  style={styles.VisitorscustomHeading}>Checked - in Visitors</Text>
     <Text style={styles.VisitorscustomCount}>{chechkedInCount}</Text>
      {/* <Text  style={styles.VisitorscustomCount}>50</Text> */}
      <TouchableOpacity onPress={() => chechkInVisitor()}>
        <Text  style={styles.visitorBtn}>VIEW</Text>
      </TouchableOpacity>            
    </View>
    <View  style={styles.customCol} >
      <Text  style={styles.VisitorscustomHeading}>Checked - out Visitors</Text>
    <Text style={styles.VisitorscustomCount}>{chechkedOutCount}</Text>
      <TouchableOpacity onPress={() => chechkoutVisitor()}>
        <Text  style={styles.visitorBtn}>VIEW</Text>
      </TouchableOpacity>            
    </View>
</View>
  );
}

const styles = StyleSheet.create({
  container: {  
    flex: 1,
        backgroundColor: '#f1f2f6',
        justifyContent: 'flex-start',
        alignItems: 'center',
        
  },

  customHeading: {
    marginTop:20,
    fontSize:20,
    textAlign:"center",
    marginBottom:20
  },  
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  errorMessage: {
      fontSize: 25,
      color: '#B53471',
      marginLeft: 15,
      marginTop: 15,
    },    
  
    section2: {       
      flexDirection:"row",
      width: "96%",           
      paddingTop:0,
      paddingBottom:0,
      borderRadius:20,       
      marginTop:30,
      marginBottom:0,
      alignItems:"center",      

    },
    customCol: {
      borderRadius:8,
      elevation: 5,
      backgroundColor: '#130f40',
      width: "30%",
      paddingLeft:"3%",
      paddingRight:"2%",
      marginTop:0,
      marginRight:"2%",
      marginLeft:"2%",
      padding:20,
      marginBottom:0,
      alignItems:'center',
      justifyContent:'center',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.3,
      shadowRadius: 1,  
    },
    VisitorscustomHeading: {
      height:50,
      width: "100%",
      fontSize: 14,        
      textAlign:'center',
      color: '#fff',      
      fontWeight:"bold",      
    },
    VisitorscustomCount: {
      fontSize: 30,
      width: "100%",
      textAlign:'center',
      marginTop:20,
      height:50,
      alignItems:'center',
      color: '#fff',
     
      fontWeight:"bold",
    },
    visitorBtn: {
      borderWidth: 1,
      borderColor:"#fff",
      width:"100%",
      padding:10,        
      borderRadius:8,
      alignItems:"center",
      justifyContent:"center",       
      marginTop:10,
      color: '#fff',
    },
});


export default VisitorCount;