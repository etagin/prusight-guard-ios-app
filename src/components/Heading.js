import React from 'react';
import {StyleSheet, Text} from 'react-native';

export function Heading({children, style, ...props}){
return <Text {...props} style={[styles.text, style]}> {children} </Text>
}

const styles = StyleSheet.create({
    text: {
      // backgroundColor: '#353b48',
      fontSize:16,
      paddingBottom:20,
      color: '#fff', 
      fontWeight: 'bold',
      alignContent: 'center'     ,
      textAlign:'center',
      paddingTop: 20
    }
  });
  
export default Heading;