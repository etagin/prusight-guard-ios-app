import React from 'react';
import {StyleSheet, Text, TextInput} from 'react-native';

export function Input({style, ...props}){
return <TextInput {...props} style={[style, styles.text]} />;
}

const styles = StyleSheet.create({
    text: {
      fontSize:16,
      paddingBottom:20,
      color: 'black',      
    }
  });
  
